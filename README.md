![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

# Testing app

APP: https://fmabp.gitlab.io/code-selection/test.html

___

# Parameters

List of parameters to start the app:

- divParent --> div's id where the app is shown.
- listCharacters --> List of characters to do the goal code.
- listCharactersCloud --> List of characters to select.
- sizeCode --> Number of characters of goal code.
- duration --> duration in seconds of animations.
- width --> width of app.
- height --> height of app.
- timeEnd --> Time to finish the app in seconds.
- maxLevel --> Maximun level. In each level the numbers of characters in cloud is increased.

___

# Meaning of fields

List of field return by getInputs:

- app_code -->  Last code to complete.
- app_codes -->  List of codes.
- app_codeInput -->  Last code inputs by participant.
- app_codesInput -->  List of codes input by participant.
- app_codesInputCorrect -->  List of right or mistakes codes (1 --> right, 0--> mistake).
- app_codesInputTimes -->  List of times for each code input in seconds.
- app_codesInputTimeNow -->  Last time of last code in seconds.
- app_codesInputTimeOneCharacter -->  List of times for each click in one character.
- app_codeLevel --> Last leel of last code.
- app_codesLevel --> List of levels for each code.
- app_code_maxLevel --> Maximum level of difficulty.
- app_code_timeEnd --> Time total of game.

---

# Qualtrics

There is a version of game for qualtrics in folder qualtrics.

- step 0 to add fields to emmbebed data.
- Step 1 to copy code.
- Step 2 to add question on qualtrics with only text.
- Step 3 to add code in js option.
