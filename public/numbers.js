function addStructureHtml(divParent,w,h){
    /* add structure html to main div */
    let zoneInput='<div id="'+divParent+'_inputs" ></div>';
    let zoneBoard='<div id="'+divParent+'_board"><svg id="'+divParent+'_svgBoardCodeGoal" width="'+w+'px" height="'+h/3+'px" style="border:2px double;margin:1% 0 1% 0;display:block;"></svg><svg id="'+divParent+'_numbersBoard" width="'+w+'px" height="'+h+'px" style="border:2px double;margin:1% 0 1% 0;display:block;"></svg><svg id="'+divParent+'_numbersCorrect" width="'+w+'px" height="'+h/3+'px" style="border:2px double;margin:1% 0 1% 0;display:block;"></svg><svg id="'+divParent+'_svgBoardCodeGoalTime" width="'+w+'px" height="'+h/3+'px" style="border:2px double;margin:1% 0 1% 0;display:block;"></svg><svg id="'+divParent+'_svgBoardInfo" width="'+w+'px" height="'+h/3+'px" style="border:2px double;margin:1% 0 1% 0;display:block;"></svg></div>';
    let parentDiv=document.getElementById(divParent);
    parentDiv.innerHTML=zoneInput+zoneBoard;
    addInputs(divParent);
}
function addInputs(divParent){
    /* add inputs */
    let inputDivName=divParent+'_inputs';
    let inputsDiv=document.getElementById(inputDivName);
    let maxLevel='<input type="hidden" id="'+inputDivName+'_code_maxLevel" value=""/>';
    let timeEnd='<input type="hidden" id="'+inputDivName+'_code_timeEnd" value=""/>';
    let code='<input type="hidden" id="'+inputDivName+'_code" value=""/>';
    let codes='<input type="hidden" id="'+inputDivName+'_codes" value=""/>';
    let codeInput='<input type="hidden" id="'+inputDivName+'_codeInput" value=""/>';
    let codesInput='<input type="hidden" id="'+inputDivName+'_codesInput" value=""/>';
    let codeLevel='<input type="hidden" id="'+inputDivName+'_codeLevel" value=""/>';
    let codesLevel='<input type="hidden" id="'+inputDivName+'_codesLevel" value=""/>';
    let codesInputCorrect='<input type="hidden" id="'+inputDivName+'_codesInputCorrect" value=""/>';//1 correct, 0 incorrect
    let codesInputTimes='<input type="hidden" id="'+inputDivName+'_codesInputTimes" value=""/>';
    let codesInputTimeNow='<input type="hidden" id="'+inputDivName+'_codesInputTimeNow" value=""/>';
    let codesInputTimesOneCharacter='<input type="hidden" id="'+inputDivName+'_codesInputTimeOneCharacter" value=""/>';


    inputsDiv.innerHTML=maxLevel+timeEnd+code+codes+codeInput+codesInput+codeLevel+codesLevel+codesInputCorrect+codesInputTimes+codesInputTimeNow+codesInputTimesOneCharacter;

}
function getInputs(divParent){
    /* Get value of inputs and return a dictionary */
    let inputDivName=divParent+'_inputs';
    let codeName=inputDivName+'_code';
    let codeNameDiv=document.getElementById(codeName);
    let codeMaxLevelName=inputDivName+'_code_maxLevel';
    let codeMaxLevelNameDiv=document.getElementById(codeMaxLevelName);
    let codeTimeEndName=inputDivName+'_code_timeEnd';
    let codeTimeEndNameDiv=document.getElementById(codeTimeEndName);
    let codesName=inputDivName+'_codes';
    let codesNameDiv=document.getElementById(codesName);
    let codeInputName=inputDivName+'_codeInput';
    let codeInputNameDiv=document.getElementById(codeInputName);
    let codesInputName=inputDivName+'_codesInput';
    let codesInputNameDiv=document.getElementById(codesInputName);
    let codeLevelName=inputDivName+'_codeLevel';
    let codeLevelNameDiv=document.getElementById(codeLevelName);
    let codesLevelName=inputDivName+'_codesLevel';
    let codesLevelNameDiv=document.getElementById(codesLevelName);
    let codesInputCorrectName=inputDivName+'_codesInputCorrect';
    let codesInputCorrectNameDiv=document.getElementById(codesInputCorrectName);
    let codesInputTimesName=inputDivName+'_codesInputTimes';
    let codesInputTimesNameDiv=document.getElementById(codesInputTimesName);
    let codesInputTimeNowName=inputDivName+'_codesInputTimeNow';
    let codesInputTimeNowNameDiv=document.getElementById(codesInputTimeNowName);
    let codesInputTimesOneCharacterName=inputDivName+'_codesInputTimeOneCharacter';
    let codesInputTimesOneCharacterNameDiv=document.getElementById(codesInputTimesOneCharacterName);
    let output={"app_code_maxLevel":codeMaxLevelNameDiv.value,"app_code_timeEnd":codeTimeEndNameDiv.value,"app_code":codeNameDiv.value,"app_codes":codesNameDiv.value,"app_codeInput":codeInputNameDiv.value,"app_codesInput":codesInputNameDiv.value,"app_codeLevel":codeLevelNameDiv.value,"app_codesLevel":codesLevelNameDiv.value,"app_codesInputCorrect":codesInputCorrectNameDiv.value,"app_codesInputTimes":codesInputTimesNameDiv.value,"app_codesInputTimeNow":codesInputTimeNowNameDiv.value,"app_codesInputTimesOneCharacter":codesInputTimesOneCharacterNameDiv.value};
    return output;
}
function countTime(divParent,timeInit,timeEnd) {
    /* this function will be updated each x time*/
    let inputNameTimeNow=divParent+'_inputs'+'_codesInputTimeNow';
    let inputTimeNow=document.getElementById(inputNameTimeNow);

    let svgNameTimeNow=divParent+'_svgBoardCodeGoalTime';
    let svgTimeNow=document.getElementById(svgNameTimeNow);

    let boardW=svgTimeNow.clientWidth;
    let boardH=svgTimeNow.clientHeight;
    let centreW=boardW/2;
    let centreH=boardH/2;

    let dateNow=new Date();
    //get diff in milisec
    let diffsec = (dateNow.getTime() - timeInit)/1000;
    if (diffsec<timeEnd){
	let minutes = parseInt(diffsec / 60)
	let seconds = parseInt(diffsec-minutes*60);
	if (minutes < 10){
	    minutes="0"+minutes;
	}
	if (seconds < 10){
	    seconds="0"+seconds;
	}
	let textTime = minutes + ":" + seconds;
	let textName=divParent+'_text'+svgNameTimeNow;
	let textNameAnimateX=textName+'_AnimateX';
	let textNameAnimateY=textName+'_AnimateY';
	let text='<text  id="'+textName+'" x="'+centreW+'" y="'+centreH+'" text-anchor="middle" stroke="black" stroke-width="2px" dy=".3em" font-size="2em" ><animate attributeName="x" id="'+textNameAnimateX+'"/><animate attributeName="y" id="'+textNameAnimateX+'"/>'+textTime+'</text>';
	svgTimeNow.innerHTML=text;
	inputTimeNow.value=parseInt(diffsec);
    }else{
	//go screen finish
	let parentDivName=divParent+'_board';
	let parentDiv=document.getElementById(parentDivName);
	let datas=getInputs(divParent);
	parentDiv.innerHTML=screenFinish(datas);
	clearInterval(window.intervalCD);

    }
    return true;
}
function getLevel(divParent,listCharacters){
    /* return level and number ofelements*/
    /* get time, timeEnd, maxLevel */
    let inputDivName=divParent+'_inputs';
    let codeName=inputDivName+'_code';
    let codeNameDiv=document.getElementById(codeName);
    let codeMaxLevelName=inputDivName+'_code_maxLevel';
    let codeMaxLevelNameDiv=document.getElementById(codeMaxLevelName);
    let codeTimeEndName=inputDivName+'_code_timeEnd';
    let codeTimeEndNameDiv=document.getElementById(codeTimeEndName);
    let codesInputTimeNowName=inputDivName+'_codesInputTimeNow';
    let codesInputTimeNowNameDiv=document.getElementById(codesInputTimeNowName);
    if (codesInputTimeNowNameDiv.value =="" ){
	codesInputTimeNowNameDiv.value="0";
    }
    let timeNow=parseInt(codesInputTimeNowNameDiv.value);
    let timeEnd=parseInt(codeTimeEndNameDiv.value);
    let maxLevel=parseInt(codeMaxLevelNameDiv.value);

    /* calculate level number of elements respect percent in character board */
    let sizeLevelTime=timeEnd/maxLevel;
    let level=parseInt(timeNow/sizeLevelTime)+1;
    let numberElements=parseInt(listCharacters.length/(maxLevel-level+1));
    return [level,numberElements];
}
function screenFinish(datas){
    /*code html to show in a table the datas*/
    let table="<table>";
    let head="<thead><tr><th>Code</th><th>Code input</th><th>Correct</th><th>Level</th><th>Time</th></tr></thead>";
    let codes=datas["app_codes"].split(";");
    let codesInput=datas["app_codesInput"].split(";");
    let codesLevel=datas["app_codesLevel"].split(";");

    let codesInputCorrect=datas["app_codesInputCorrect"].split(";");
    let codesTime=datas["app_codesInputTimes"].split(";");
    let dataTable="<tbody>";
    for (let i=1;i<codes.length;i++){
	let codeInput=codesInput[i];
	if (codesInput.length-1<i && i>1){
	    codeInput= "--";
	}
	let codeLevel=codesLevel[i];
	if (codesLevel.length-1<i && i>1){
	    codeLevel= "--";
	}
	let correct="YES";
	if (codesInputCorrect.length-1<i || codesInputCorrect[i]=="0"){
	    correct="NOT";
	}
	let timeInThisCode=parseInt(codesTime[i]);
	if (codesInputCorrect.length-1>=i && i>1){
	    timeInThisCode=parseInt(codesTime[i])-parseInt(codesTime[i-1]);
	}
	if (codesInputCorrect.length-1<i && i>1){
	    timeInThisCode= "--";
	}
	let row="<tr><td>"+codes[i]+"</td>"+"<td>"+codeInput+"</td>"+"<td>"+correct+"</td>"+"<td>"+codeLevel+"</td>"+"<td>"+timeInThisCode+"</td></tr>";
	dataTable=dataTable+row;
    }
    table=table+head+dataTable+"</tbody></table>";
    return table;
}
function selectRandomCharacter(listCharacters, listCharactersSelected){
    /* choose one character randomly not repeated */
    let notRepeated=true;
    let numberSelected=null;
    while (notRepeated){
	numberSelected= listCharacters[Math.floor(Math.random() * listCharacters.length)];
	if (listCharactersSelected.includes(numberSelected)){
	    notRepeated=true;
	}else{
	    notRepeated=false;
	}
    }
    return numberSelected;
}
function calculatePosCircleCenterRandom(listCharacters,svgBoard){
    /* choose new postion randomly in circle in centre*/
    let posEnd=new Array();
    /* draw all characters around of centre of board */
    let board=document.getElementById(svgBoard);
    //calculate radious of circles for characteres
    let boardW=board.clientWidth;
    let boardH=board.clientHeight;
    let minSide=boardW;
    if (boardW>boardH){
	minSide=boardH;
    }
    let radioCircle=minSide/(17);
    let centreW=boardW/2;
    let centreH=boardH/2;
    let circles='';
    for (let i=0;i<listCharacters.length;i++){
	let posCX=Math.random()*(4*radioCircle)-2*radioCircle+centreW;
	let posCY=Math.random()*(4*radioCircle)-2*radioCircle+centreH;
	posEnd.push([posCX,posCY]);
    }
    return posEnd;

}
function calculatePosCirclesRandom(listCharacters,svgBoard){
    /* choose new postion randomly in circle*/
    let posEnd=new Array();
    /* draw all characters around of centre of board */
    let board=document.getElementById(svgBoard);
    //calculate radious of circles for characteres
    let boardW=board.clientWidth;
    let boardH=board.clientHeight;
    let minSide=boardW;
    if (boardW>boardH){
	minSide=boardH;
    }
    let radioCircle=minSide/(17);
    let centreW=boardW/2;
    let centreH=boardH/2;
    let circles='';
    let noAccept=true;
    let posCX=null;
    let posCY=null;
    let distance=0;
    for (let i=0;i<listCharacters.length;i++){
	noAccept=true;
	while (noAccept){
	    posCX=Math.random()*(boardW-2*radioCircle)+radioCircle;
	    posCY=Math.random()*(boardH-2*radioCircle)+radioCircle;
	    noAccept=false;
	    for (let j=0;j<posEnd.length;j++){
		distance=Math.pow(Math.pow(posEnd[j][0]-posCX,2)+Math.pow(posEnd[j][1]-posCY,2),0.5);
		if (distance<2*radioCircle){
		    noAccept=true;
		}
	    }
	}
	posEnd.push([posCX,posCY]);
    }
    return posEnd;

}
function drawAllCharactersBoard(listCharacters,svgBoard,divParent){
    /* draw all characters around of centre of board */
    let board=document.getElementById(svgBoard);
    let codeGoalName=divParent+'_svgBoardCodeGoal';
    let codeGoal=document.getElementById(codeGoalName);
    //calculate radious of circles for characteres
    let boardW=board.clientWidth;
    let boardH=board.clientHeight;
    let minSide=boardW;
    if (boardW>boardH){
	minSide=boardH;
    }
    let radioCircle=minSide/(17);
    let centreW=boardW/2;
    let centreH=boardH/2;
    let circles='';
    let codesGoal='';
    for (let i=0;i<=listCharacters.length;i++){

	let posCX=-100;
	let posCY=-100;
	let posTX=posCX;
	let posTY=posCY;
	let circleName=divParent+'_circleBoard_'+i+'_'+listCharacters[i];
	let circleNameAnimateX=circleName+'_AnimateX';
	let circleNameAnimateY=circleName+'_AnimateY';
	let textName=divParent+'_textBoard_'+i+'_'+listCharacters[i];
	let textNameAnimateX=textName+'_AnimateX';
	let textNameAnimateY=textName+'_AnimateY';

	let circle='<circle id="'+circleName+'" cx="'+posCX+'" cy="'+posCY+'" r="'+radioCircle+'" stroke="black" stroke-width="1" fill="black" ><animate attributeName="cx" id="'+circleNameAnimateX+'"/><animate attributeName="cy" id="'+circleNameAnimateY+'"/></circle> ';
	let text='<text  id="'+textName+'" x="'+posTX+'" y="'+posTY+'" text-anchor="middle" stroke="white" stroke-width="2px" dy=".3em"  >'+listCharacters[i]+'<animate attributeName="x" id="'+textNameAnimateX+'"/><animate attributeName="y" id="'+textNameAnimateY+'"/></text>';
	circles=circles+circle+text;
	let textCodeGoalName=codeGoalName+'_textBoard'+listCharacters[i];
	let textCodeGoalNameAnimateX=textCodeGoalName+'_AnimateX';
	let textCodeGoalNameAnimateY=textCodeGoalName+'_AnimateY';
	let textCodeGoal='<text  id="'+textCodeGoalName+'" x="-100" y="-100" text-anchor="middle" stroke="black" stroke-width="2px" dy=".3em" font-size="2em" ><animate attributeName="x" id="'+textCodeGoalNameAnimateX+'"/><animate attributeName="y" id="'+textCodeGoalNameAnimateY+'"/>'+listCharacters[i]+'</text>';
	codesGoal=codesGoal+textCodeGoal;

    }
    board.innerHTML=circles;
    codeGoal.innerHTML=codesGoal;

}
function moveCircle(circleName,circleNameAnimateX,circleNameAnimateY,posOrig,posEnd,dur,begin){
    /* move circle */
    let circle=document.getElementById(circleName);
    let circleAnimateX=document.getElementById(circleNameAnimateX);
    let circleAnimateY=document.getElementById(circleNameAnimateY);
    circle.setAttributeNS(null,"cx",posEnd[0]);
    circle.setAttributeNS(null,"cy",posEnd[1]);
    circleAnimateY.setAttribute("attributeName","cy");
    circleAnimateY.setAttribute("from",posOrig[1]);
    circleAnimateY.setAttribute("id",circleNameAnimateX);
    circleAnimateY.setAttribute("to",posEnd[1]);
    circleAnimateY.setAttribute("dur",dur);
    circleAnimateY.setAttribute("begin",begin);
    circleAnimateY.beginElement();
    circleAnimateX.setAttribute("attributeName","cx");
    circleAnimateX.setAttribute("id",circleNameAnimateY);
    circleAnimateX.setAttribute("from",posOrig[0]);
    circleAnimateX.setAttribute("to",posEnd[0]);
    circleAnimateX.setAttribute("dur",dur);
    circleAnimateX.setAttribute("begin",begin);
    circleAnimateX.beginElement();


}
function moveText(textName,textNameAnimateX,textNameAnimateY,posOrig,posEnd,dur,begin){
    /* move text */
    let text=document.getElementById(textName);
    let textAnimateX=document.getElementById(textNameAnimateX);
    let textAnimateY=document.getElementById(textNameAnimateY);
    text.setAttributeNS(null,"x",posEnd[0]);
    text.setAttributeNS(null,"y",posEnd[1]);
    textAnimateY.setAttribute("attributeName","y");
    textAnimateY.setAttribute("from",posOrig[1]);
    textAnimateY.setAttribute("id",textNameAnimateY);
    textAnimateY.setAttribute("to",posEnd[1]);
    textAnimateY.setAttribute("dur",dur);
    textAnimateY.setAttribute("begin",begin);
    textAnimateY.beginElement();
    textAnimateX.setAttribute("attributeName","x");
    textAnimateX.setAttribute("id",textNameAnimateX);
    textAnimateX.setAttribute("from",posOrig[0]);
    textAnimateX.setAttribute("to",posEnd[0]);
    textAnimateX.setAttribute("dur",dur);
    textAnimateX.setAttribute("begin",begin);
    textAnimateX.beginElement();

}

function selectCode(divParent,sizeCode,listCharacters,svgBoard,duration){
    /* select code from character list */
    let codeGoalName=divParent+'_svgBoardCodeGoal';
    let codeGoal=document.getElementById(codeGoalName);
    let boardTextW=codeGoal.clientWidth;
    let boardTextH=codeGoal.clientHeight;


    let listCharactersSelected=[];
    let code="";
    let inputDivName=divParent+'_inputs';
    let inputsDiv=document.getElementById(inputDivName);
    //put circles in initial pos
    let board=document.getElementById(svgBoard);
    let boardW=board.clientWidth;
    let boardH=board.clientHeight;

    let dur=duration+'s';

    for (let i=0;i<sizeCode;i++){
	// get new character
	let newCharacter=selectRandomCharacter(listCharacters, listCharactersSelected);
	//add new character to selected
	listCharactersSelected.push(newCharacter);
	code=code+newCharacter;
	//animate cicle of character

	let begin=i+"s";
	let circleName=divParent+'_circleBoard_'+0+'_'+listCharacters[0];
	let circleNameAnimateX=divParent+'_circleBoard_'+i+'_'+newCharacter+'_AnimateX';
	let circleNameAnimateY=divParent+'_circleBoard_'+i+'_'+newCharacter+'_AnimateY';
	let textName=divParent+'_textBoard_'+i+'_'+newCharacter;
	let textNameAnimateX=divParent+'_textBoard_'+i+'_'+newCharacter+'_AnimateX';
	let textNameAnimateY=divParent+'_textBoard_'+i+'_'+newCharacter+'_AnimateY';

	//let text=document.getElementById(textName);

	//let textAnimateX=document.getElementById(textNameAnimateX);
	//let textAnimateY=document.getElementById(textNameAnimateY);
	//calculate pos finish
	let circle=document.getElementById(circleName);

	let x=circle.getAttributeNS(null,"cx");
	let y=circle.getAttributeNS(null,"cy");
	let r=circle.getAttributeNS(null,"r");
	let xEnd=boardW/2+2*r*(i-(sizeCode-1)/2)-3;
	let yEnd=r;
	/*let posOrig=[x,y];
	let posEnd=[xEnd,yEnd];*/
	//moveCircle(circleName,circleNameAnimateX,circleNameAnimateY,posOrig,posEnd,dur,begin);

	//moveText(textName,textNameAnimateX,textNameAnimateY,posOrig,posEnd,dur,begin);

	//add codeGoal
	let textCodeGoalName=codeGoalName+'_textBoard'+newCharacter;
	let textCodeGoalNameAnimateX=textCodeGoalName+'_AnimateX';
	let textCodeGoalNameAnimateY=textCodeGoalName+'_AnimateY';
	let beginCodeGoal=dur;
	moveText(textCodeGoalName,textCodeGoalNameAnimateX,textCodeGoalNameAnimateY,[xEnd,boardTextH],[xEnd,boardTextH/2],dur,beginCodeGoal);
    }
    let codeName=inputDivName+'_code';
    let codesName=inputDivName+'_codes';
    let codeInput=document.getElementById(codeName);
    let codesInput=document.getElementById(codesName);
    codeInput.value=code;
    codesInput.value=codesInput.value+";"+code;


    return code;
}
function moveAllCircles(divParent,svgBoard,listCharacters,posEnd,duration){
    /* replace randomly the circles */
    //put circles in initial pos
    let board=document.getElementById(svgBoard);

    let boardW=board.clientWidth;
    let boardH=board.clientHeight;

    let dur=1+'s';
    let begin="0s";

    for (let i=0;i<listCharacters.length;i++){


	let circleName=divParent+'_circleBoard_'+i+'_'+listCharacters[i];
	let circleNameAnimateX=divParent+'_circleBoard_'+i+'_'+listCharacters[i]+'_AnimateX';
	let circleNameAnimateY=divParent+'_circleBoard_'+i+'_'+listCharacters[i]+'_AnimateY';
	let textName=divParent+'_textBoard_'+i+'_'+listCharacters[i];
	let textNameAnimateX=divParent+'_textBoard_'+i+'_'+listCharacters[i]+'_AnimateX';
	let textNameAnimateY=divParent+'_textBoard_'+i+'_'+listCharacters[i]+'_AnimateY';

	let text=document.getElementById(textName);

	let textAnimateX=document.getElementById(textNameAnimateX);
	let textAnimateY=document.getElementById(textNameAnimateY);
	//calculate pos finish
	let circle=document.getElementById(circleName);
	let x=circle.getAttributeNS(null,"cx");
	let y=circle.getAttributeNS(null,"cy");
	let xEnd=posEnd[i][0];
	let yEnd=posEnd[i][1];
	let posOrig=[x,y];

	moveCircle(circleName,circleNameAnimateX,circleNameAnimateY,posOrig,[xEnd,yEnd],dur,begin);

	moveText(textName,textNameAnimateX,textNameAnimateY,posOrig,[xEnd,yEnd],dur,begin);
    }

}
function addTextToBoardInfoScene1(divParent,text,duration){
    /* add text with info to board info */
    let boardInfoName=divParent+'_svgBoardInfo';
    let boardInfo=document.getElementById(boardInfoName);
    let boardW=boardInfo.clientWidth;
    let boardH=boardInfo.clientHeight;
    //remove all elements
    boardInfo.innerHTML = "";
    //add text
    let x= boardW/2;
    let y=boardH/2;
    let fontSize=boardW/250;
    let barInfo='<rect width="100%" height="100%" fill="green" ><animate attributeName="width" from="0%" to="100%" dur="'+duration+'s" begin="0s" /></rect> ';
    let textInfo='<text  x="'+x+'" y="'+y+'" text-anchor="middle" stroke="black" stroke-width="2px" dy=".3em"  font-size="'+fontSize+'em">'+text+'</text>';
    boardInfo.innerHTML=barInfo+textInfo;

}
function getCorrectError(divParent){
    /* get amount of codes corrects and errors */
    let inputDivName=divParent+'_inputs';
    let codesInputCorrectName=inputDivName+'_codesInputCorrect';
    let codesInputCorrect=document.getElementById(codesInputCorrectName);
    let corrects=0;
    let errors=0;
    let codesInputCorrectValue=codesInputCorrect.value;
    if (codesInputCorrectValue==null || codesInputCorrectValue==""){
	corrects=0;
	errors=0;
    }else{
	let codesInputCorrectList = codesInputCorrectValue.split(";");
	for (let i=1;i<codesInputCorrectList.length+1;i++){
	    let codeInputCorrect=parseInt(codesInputCorrectList[i]);
	    if (codeInputCorrect==0){
		errors=errors+1;
	    }
	    if (codeInputCorrect==1){
		corrects=corrects+1;
	    }
	}
    }
    return [corrects,errors];

}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
function selectElementsToLevel(divParent,listCharacters){
    /* select elements of list characters*/
    let levelInfo=getLevel(divParent,listCharacters);
    let level=levelInfo[0];
    let inputDivName=divParent+'_inputs';
    let codeLevelName=inputDivName+'_codeLevel';
    let codeLevelNameDiv=document.getElementById(codeLevelName);
    let codesLevelName=inputDivName+'_codesLevel';
    let codesLevelNameDiv=document.getElementById(codesLevelName);
    codeLevelNameDiv.value=level;
    let numberElements=levelInfo[1];
    codesLevelNameDiv.value=codesLevelNameDiv.value+";"+level;
    let listElement=new Array();
    for (let i=0;i<numberElements;i++){
	listElement.push(listCharacters[i]);
    }
    return listElement;
}
function scene1(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration){
    /* scene 1 */
    // get list charactheres to level */
    let listElements=selectElementsToLevel(divParent,listCharactersCloud);
    console.log(listCharactersCloud,listElements);
    drawAllCharactersBoard(listElements,svgBoard,divParent);

    //put circles in initial pos
    addTextToBoardInfoScene1(divParent,"GET RANDOM CODE",duration*4);
    let posEnd=calculatePosCircleCenterRandom(listElements,svgBoard);
    moveAllCircles(divParent,svgBoard,listElements,posEnd,duration);

    //moveAllCircles(divParent,svgBoard,listCharacters,posEnd)

    sleep(duration*1.5*1000).then(() => {
	selectCode(divParent,sizeCode,listCharacters,svgBoard,duration);
	sleep(duration*1.5*1000).then(() => {
	    posEnd=calculatePosCirclesRandom(listElements,svgBoard);
	    moveAllCircles(divParent,svgBoard,listElements,posEnd);
	    sleep(duration*1.0*1000).then(() => {
		scene2(listCharacters,listElements,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
	    });
	});

    });

}
function addTextToBoardCorrectError(divParent){
    /* add text with info about corrects and errors to board info */
    let boardInfoName=divParent+'_svgBoardInfo';
    let boardInfo=document.getElementById(boardInfoName);
    let boardW=boardInfo.clientWidth;
    let boardH=boardInfo.clientHeight;
    let correctsErrors=getCorrectError(divParent);
    //remove all elements
    boardInfo.innerHTML = "";
    //add text

    let x= 10;
    let y=boardH/2;
    let fontSize=boardW/250;
    let CorrectInfo='<text width="40%" height="100%" text-anchor="start"  x="5%" y="'+y+'"  stroke="black" stroke-width="2px" dy=".3em"  font-size="'+fontSize+'em" xml:space="preserve" >Corrects '+correctsErrors[0]+'</text> ';
    let ErrorInfo='<text width="40%" height="100%" text-anchor="end" x="90%" y="'+y+'"  stroke="black" stroke-width="2px" dy=".3em"  font-size="'+fontSize+'em" xml:space="preserve" >Errors '+correctsErrors[1]+'</text> ';

    boardInfo.innerHTML=CorrectInfo+ErrorInfo;

}
function addClickCircle(divParent,listCharacters,listElements,listCharactersCloud,sizeCode,duration){
    /* add click event to characters circles */
    for (let i=0;i<listElements.length;i++){


	let circleName=divParent+'_circleBoard_'+i+'_'+listCharactersCloud[i];
	let circle=document.getElementById(circleName);
	let textName=divParent+'_textBoard_'+i+'_'+listCharactersCloud[i];
	let text=document.getElementById(textName);

	circle.addEventListener("click", function(){
	    clickCode(divParent,listElements[i],listCharacters,listCharactersCloud,sizeCode,duration)
	},false);
	text.addEventListener("click", function(){
	    clickCode(divParent,listElements[i],listCharacters,listCharactersCloud,sizeCode,duration)
	},false);

    }
}

function clickCode(divParent,selectCharacter,listCharacters,listCharactersCloud,sizeCode,duration){
    /* click in circle to select code and change color circle*/
    let circleName=divParent+'_circleBoard_'+listCharactersCloud.indexOf(selectCharacter)+'_'+selectCharacter;
    let circle=document.getElementById(circleName);
    // time

    let codesInputTimeNow=divParent+'_inputs'+'_codesInputTimeNow';
    let codesInputTimeNowValue=document.getElementById(codesInputTimeNow);
    let codesInputTimes=divParent+'_inputs'+'_codesInputTimes';
    let codesInputTimesValue=document.getElementById(codesInputTimes);
    let codesInputTimesOneCharacter=divParent+'_inputs'+'_codesInputTimeOneCharacter';
    let codesInputTimesOneCharacterValue=document.getElementById(codesInputTimesOneCharacter);
    let dateNow=new Date();
    //get diff in milisec
    let sec = parseInt(codesInputTimeNowValue.value);
    codesInputTimesOneCharacterValue.value=codesInputTimesOneCharacterValue.value+";"+sec;
    // add code to codeInput
    let codeInputName=divParent+'_inputs'+'_codeInput';
    let codeInput=document.getElementById(codeInputName);
    let codesInputName=divParent+'_inputs'+'_codesInput';
    let codesInput=document.getElementById(codesInputName);
    //check if wrong o correct
    codeInput.value=codeInput.value+selectCharacter;
    let lengthInputCode=codeInput.value.length;
    let codeGoalName=divParent+'_inputs'+'_code';
    let codeGoalInput=document.getElementById(codeGoalName);

    let codesInputCorrectErrorName=divParent+'_inputs'+'_codesInputCorrect';//1 correct, 0 incorrect
    let codesInputCorrectError=document.getElementById(codesInputCorrectErrorName);
    let right=true;
    if ( lengthInputCode<=codeGoalInput.value.length){
	if (codeGoalInput.value[lengthInputCode-1]==selectCharacter){

	    //right
	    circle.setAttributeNS(null,"fill","orange");
	    addSelectCodeToBoard(divParent,selectCharacter,lengthInputCode, codeGoalInput.value.length,"green");

	}else{
	    //wrong
	    right=false;
	    circle.setAttributeNS(null,"fill","red");
	    addSelectCodeToBoard(divParent,selectCharacter,lengthInputCode, codeGoalInput.value.length,"red");
	    codesInput.value=codesInput.value+";"+codeInput.value;

	    codesInputCorrectError.value=codesInputCorrectError.value+";0";
	    //goto escena error
	    let svgBoard=divParent+'_numbersBoard';
	    let cloudBoard=document.getElementById(svgBoard);
	    cloudBoard.innerHTML="";
	    codesInputTimesValue.value=codesInputTimesValue.value+";"+sec;
	    sleep(duration*1.0*1000).then(() => {
		scene3(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
	    });
	}

	//if codeInput length is > 0 then link with the last
	if (lengthInputCode>1 && right ){
	    //take before circle
	    let circleBeforeName=divParent+'_circleBoard_'+listCharactersCloud.indexOf(codeInput.value[lengthInputCode-2])+'_'+codeInput.value[lengthInputCode-2];
	    let circleBefore=document.getElementById(circleBeforeName);

	    let svgBoard=divParent+'_numbersBoard';

	    let board=document.getElementById(svgBoard);
	    let xOrig=circleBefore.getAttributeNS(null,"cx");
	    let yOrig=circleBefore.getAttributeNS(null,"cy");
	    let xEnd=circle.getAttributeNS(null,"cx");
	    let yEnd=circle.getAttributeNS(null,"cy");

	    let path= 'M'+xOrig+' '+yOrig+' L'+xEnd+' '+yEnd+' Z';
	    let newElement = document.createElementNS("http://www.w3.org/2000/svg", 'path'); //Create a path in SVG's namespace
	    newElement.setAttribute("d",path); //Set path's data
	    newElement.style.stroke = "black"; //Set stroke colour
	    newElement.style.strokeWidth = "3px"; //Set stroke width
	    board.insertBefore(newElement, board.childNodes[0]);
	}

	if (lengthInputCode==codeGoalInput.value.length && right){
	    codesInputCorrectError.value=codesInputCorrectError.value+";1";

	    //goto scene right
	    let svgBoard=divParent+'_numbersBoard';

	    let cloudBoard=document.getElementById(svgBoard);
	    cloudBoard.innerHTML="";
	    codesInput.value=codesInput.value+";"+codeInput.value;
	    codesInputTimesValue.value=codesInputTimesValue.value+";"+sec;

	    sleep(duration*1.0*1000).then(() => {
		scene4(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
	    });


	}
    }

}
function addSelectCodeToBoard(divParent,selectCharacter,position,total,color){
    /* add code selected to bottom board*/

    let svgName= divParent+'_numbersCorrect';
    let svg=document.getElementById(svgName);
    let boardW=svg.clientWidth;
    let boardH=svg.clientHeight;
    let minSide=boardW;
    if (boardW>boardH){
	minSide=boardH;
    }

    let r=minSide/3;
    let x=boardW/2+6*r*((position-1)-(total-1)/2);
    let y=boardH/2;

    let circle='<circle cx="'+x+'" cy="'+y+'" r="'+r+'" stroke="blac4k" stroke-width="1" fill="'+color+'" ></circle> ';
    let text='<text  x="'+x+'" y="'+y+'" text-anchor="middle" stroke="white" stroke-width="2px" dy=".3em"  >'+selectCharacter+'</text>';
    svg.innerHTML=svg.innerHTML+circle+text;
}
function scene2(listCharacters,listElements,listCharactersCloud,divParent,svgBoard,sizeCode,duration){
    /* scene 2 */
    //put correct and error
    addTextToBoardCorrectError(divParent);
    addClickCircle(divParent,listCharacters,listElements,listCharactersCloud,sizeCode,duration)
}
function putTextError(divParent){
    /* put text "WRONG" in cloud */
    let svgBoard=divParent+'_numbersBoard';
    let cloudBoard=document.getElementById(svgBoard);
    let boardW=cloudBoard.clientWidth;
    let boardH=cloudBoard.clientHeight;
    let x=boardW/2;
    let y=boardH/2;
    let fontSize=boardW/100;
    let text='<text  x="'+x+'" y="'+y+'" text-anchor="middle" stroke="RED" stroke-width="2px" dy=".3em" font-size="'+fontSize+'em" fill="red" >WRONG</text>';
    cloudBoard.innerHTML=text;


}
function scene3(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration){
    /* scene 3 show error to complete code */
    //put  error
    addTextToBoardCorrectError(divParent);
    putTextError(divParent);
    sleep(duration*1.0*1000).then(() => {
		scene5(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
    });
}
function putTextCorrect(divParent){
    /* put text "CORRECT" in cloud */
    let svgBoard=divParent+'_numbersBoard';
    let cloudBoard=document.getElementById(svgBoard);
    let boardW=cloudBoard.clientWidth;
    let boardH=cloudBoard.clientHeight;
    let x=boardW/2;
    let y=boardH/2;
    let fontSize=boardW/100;
    let text='<text  x="'+x+'" y="'+y+'" text-anchor="middle" stroke="green" stroke-width="2px" dy=".3em" font-size="'+fontSize+'em" fill="green" >CORRECT</text>';
    cloudBoard.innerHTML=text;


}
function putTextNext(divParent){
    /* put text "CORRECT" in cloud */
    let svgBoard=divParent+'_numbersBoard';
    let cloudBoard=document.getElementById(svgBoard);
    let boardW=cloudBoard.clientWidth;
    let boardH=cloudBoard.clientHeight;
    let x=boardW/2;
    let y=boardH/2;
    let fontSize=boardW/100;
    let text='<text  x="'+x+'" y="'+y+'" text-anchor="middle" stroke="green" stroke-width="2px" dy=".3em" font-size="'+fontSize+'em" fill="green" >NEXT</text>';
    cloudBoard.innerHTML=text;


}
function scene4(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration){
    /* scene 4 show correct to complete code */
    //put  correct
    addTextToBoardCorrectError(divParent);
    putTextCorrect(divParent);
    sleep(duration*1.0*1000).then(() => {
		scene5(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
    });
}

function scene5(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration){
    /* scene 5 show nextmessage and go to scene1 */
    //put  correct
    putTextNext(divParent);
    //reset
    let inputDivName=divParent+'_inputs';

    let codeName=inputDivName+'_code';
    let codeInputName=inputDivName+'_codeInput';
    let code=document.getElementById(codeName);
    code.value="";
    let codeInput=document.getElementById(codeInputName);
    codeInput.value="";
    let svgName= divParent+'_numbersCorrect';
    let svg=document.getElementById(svgName);
    svg.innerHTML="";
    sleep(duration).then(() => {
	scene1(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
    });
}

function initBoard(){
    let divParent="app";

    let listCharacters=["Ø","1","2","3","4","5","6","7","8","9"];
    let listCharactersCloud=["Ø","1","2","3","4","5","6","7","8","9","A","E","I","O","U","B","M","P","R","T","A","E","I","O","U","B","M","P","R","T"];
    let sizeCode=3;
    let duration=1;
    let width=500;
    let elmnt = document.getElementById(divParent);
    if (elmnt.offsetWidth<width){
	width=elmnt.offsetWidth;
    }

    let height=200;

    addStructureHtml(divParent,width,height);
    let svgBoard=divParent+'_numbersBoard';
    let codeGoalName=divParent+'_svgBoardCodeGoal';
    let codeGoal=document.getElementById(codeGoalName);
    //put circles in initial pos
    let board=document.getElementById(svgBoard);
    let boardW=board.clientWidth;
    let boardH=board.clientHeight;
    //max level
    let maxLevel=2;
    // init time
    let timeEnd=300;//in seconds

    // add maxLevel and timeEnd to inputs hidden
    let inputDivName=divParent+'_inputs';
    let codeMaxLevelName=inputDivName+'_code_maxLevel';
    let codeMaxLevel=document.getElementById(codeMaxLevelName);
    codeMaxLevel.value=maxLevel;
    let codeTimeEndName=inputDivName+'_code_timeEnd';
    let codeTimeEnd=document.getElementById(codeTimeEndName);
    codeTimeEnd.value=timeEnd;
    let dateInit = new Date();
    let timeInit = dateInit.getTime();
    window.intervalCD=window.setInterval(function() {
	countTime(divParent,timeInit,timeEnd);
    }, 1000);


    sleep(duration).then(() => {
	scene1(listCharacters,listCharactersCloud,divParent,svgBoard,sizeCode,duration);
    });



}

window.onload=initBoard;
